import Vue from 'vue'
import App from './App'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios'
import './index.scss'

const base = axios.create({
  baseURL: 'http://localhost:3001'
})

Vue.mixin({
  data () {
    return {
      api: 'http://localhost:3001'
    }
  }
})

Vue.config.productionTip = false
Vue.prototype.$http = base

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
