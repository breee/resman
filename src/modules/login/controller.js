'use strict'

import bcrypt        from 'bcryptjs'
import sysLogger     from 'simple-node-logger'

import GetUser       from '../../helpers/getuser'
import User          from '../../models/user'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class UserLogin {
  constructor() {
    this.username = ''
    this.password = ''
  }

  static async authLogin (req, res, next) {

    this.username = req.body.username || ''
    this.password = req.body.password || ''

    const checkFields = (username, password) => {
      if (username === '' || password === '') {
        return Promise.reject({
          statusCode: 401,
          statusMessage: 'Unauthorized Login...'
        })
      }
      return username
    }

    const checkUser = async (username) => {
      const user = await GetUser.findUser(username)
      return user
    }

    const checkPassword = async (authUser, reqPassword, encPassword) => {
      const validPassword = await authUser.comparePassword(reqPassword, encPassword)
      return validPassword
    }

    try {

      const user          = await checkFields(this.username, this.password)
      const encryptedUser = await checkUser(user)
      const isMatch       = await checkPassword(encryptedUser, this.password, encryptedUser.password)
      delete encryptedUser.password
      if (isMatch) {
        req.jwtSession.encryptedUser = {
          username : encryptedUser.username,
          firstname: encryptedUser.firstname,
          lastname : encryptedUser.lastname,
        }

        const claims = {
          iss      : "my application name",
          aud      : "myapplication.com",
          username : encryptedUser.username
        }

        req.jwtSession.create(claims, function(error, token){
          log.info(`Access Token Issued: ${token}`)
          res.json({
            token : token,
            firstname : encryptedUser.firstname,
            lastname  : encryptedUser.lastname,
          })
        })
      }


    } catch (err) {

      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)

    }

  }
}

module.exports = UserLogin
