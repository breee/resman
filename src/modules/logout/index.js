'use strict'

import express   from 'express'
import UserLogout from './controller'
import VerifyToken from '../../config/auth'

const logoutRoute = require('node-async-router')()

logoutRoute.get('/', VerifyToken.authorizeToken, UserLogout.authLogout)

export default logoutRoute

