'use strict'

import express from 'express'
import { RegisterUser }   from './controller'
import VerifyToken from '../../config/auth'

const userRoute = require('node-async-router')()

userRoute.post('/add-user', RegisterUser.account)

export default userRoute
