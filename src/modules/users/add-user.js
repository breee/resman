'use strict'

import sysLogger from 'simple-node-logger'
import mongoose  from 'mongoose'

import User      from '../../models/user'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class RegisterUser {
  constructor() {
    this.username  = ''
    this.password  = ''
    this.firstname = ''
    this.lastname  = ''
  }

  static async account (req, res, next) {
    this.username  = req.body.username
    this.password  = req.body.password
    this.firstname = req.body.firstname
    this.lastname  = req.body.lastname

    const addUserAccount = async () => {
      let newUser = new User()
        newUser.username  = this.username
        newUser.password  = this.password
        newUser.firstname = this.firstname
        newUser.lastname  = this.lastname

        const createUser = await newUser.save()
        if (createUser) {
          return Promise.resolve(res.json({success: true, message: 'Successfully created account...'}))
        }
        return Promise.reject({
          statusCode: 400,
          statusMessage: 'Unable to create account...'
        })
    }

    try {
      await addUserAccount()
    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)
    }

  }
}

module.exports = RegisterUser
