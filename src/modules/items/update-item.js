'use strict'

import sysLogger from 'simple-node-logger'
import mongoose  from 'mongoose'

import Items     from '../../models/item'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class UpdateStock {
  constructor() {}

  static async quantity (req, res, next) {
    const itemId = req.params.id
    const updatedQuantity = req.body.data
    console.log(itemId)


    const updateItemQuantity = async () => {
      console.log('item')
      const item = await Items.findOne({ '_id': itemId })

      for (let field in updatedQuantity) {
        item[field] = updatedQuantity[field]
      }

      const updateStock = await item.save()
      if (updateStock) {
        console.log('Successfully updated stock...')
        return Promise.resolve(updatedQuantity)
      } else {
        return Promise.reject ({
          statusCode: 400,
          statusMessage: 'Unable to update stock...'
        })
      }

    }

    try {
      const updatedtemQuantity = await updateItemQuantity()
      res.json(updatedtemQuantity)
    } catch(err) {
      console.log('error update')
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)
    }

  }
}

module.exports = UpdateStock
