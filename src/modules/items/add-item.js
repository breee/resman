'use strict'

import sysLogger from 'simple-node-logger'
import mongoose  from 'mongoose'

import Item      from '../../models/item'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class CreateItem {
  constructor() {
    this.name = ''
    this.description = ''
    this.imgUrl = ''
    this.quantity = ''
  }

  static async sku (req, res, next) {
    this.name         = req.body.name
    this.description  = req.body.description
    this.imgUrl       = req.body.imgUrl
    this.quantity     = req.body.quantity

    const createItemSku = async () => {
      let newItem = new Item()
        newItem.name         = this.name
        newItem.description  = this.description
        newItem.imgUrl       = this.imgUrl
        newItem.quantity     = this.quantity

        const createItem = await newItem.save()
        if (createItem) {
          return Promise.resolve(res.json({success: true, message: 'Successfully created item...'}))
        }
        return Promise.reject({
          statusCode: 400,
          statusMessage: 'Unable to create item...'
        })
    }

    try {
      await createItemSku()
    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)
    }

  }
}

module.exports = CreateItem
