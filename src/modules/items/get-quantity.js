'use strict'

import sysLogger from 'simple-node-logger'
import mongoose  from 'mongoose'

import Items     from '../../models/item'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class GetItemQuantity {
  constructor() {}

  static async quantity (req, res, next) {
    const itemId = req.params.id

    const getItemQuantity = async () => {
      const item = await Items.findOne({ _id: itemId })
      if (!item || item == undefined) {
      return Promise.reject ({
          statusCode: 401,
          statusMessage: 'Items not found...'
        })
      }
      return Promise.resolve(item)
    }

    try {
      const itemQuantity = await getItemQuantity()
      res.json(itemQuantity.quantity)
    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)
    }

  }
}

module.exports = GetItemQuantity
