'use strict'

import jwt       from 'jsonwebtoken'
import path      from 'path'
import sysLogger from 'simple-node-logger'

import GetUser   from '../helpers/getuser'

import { pub } from '../authkey'
// import User    from '../models/user'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class VerifyToken {
  constructor(pub) {
    this.tokenKey = pub
  }

  static async authorizeToken (req, res, next) {
    const parse = req.headers
    const { 'x-access-token': xAccessToken } = parse

    const checkTokenHeader = (reqToken) => {

      if (reqToken) {
        return reqToken
      }
      return Promise.reject({
        statusCode: 403,
        statusMessage: 'No token provided...'
      })
    }

    const verifyToken = async (userToken) => {
      const decoded = await jwt.verify(userToken, pub)
      if (decoded) {
        return decoded.username
      }
      return Promise.reject({
        statusCode: 401,
        statusMessage: 'Failed to authenticate token...'
      })
    }

    const checkUser = async (username) => {
      const user = await GetUser.findUser(username)
      if (!user) {
        return Promise.reject({
          statusCode: 403,
          statusMessage: 'Authentication failed. User not found'
        })
      }
      return user
    }

    try {
      const confirmToken = await checkTokenHeader(xAccessToken.toString())
      const decodedToken = await verifyToken(confirmToken)
      const validUser    = await checkUser(decodedToken)
      if (validUser && req.jwtSession.id) {
        log.info("Establish a verified a JWT session:" + req.jwtSession.encryptedUser.firstname + " " + req.jwtSession.encryptedUser.lastname + " is accessing the route with")
        log.info("Request JWT session data: ",
        req.jwtSession.id,
        req.jwtSession.claims,
        req.jwtSession.jwt
        )
      } else {
        await Promise.reject({
          statusCode: 401,
          statusMessage: 'No session establish...'
        })
      }
      next()


    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)

    }
  }
}

module.exports = VerifyToken
