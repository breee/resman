'use strict'

import mongoose from 'mongoose'
import User     from '../models/user'

class GetUser {
  static async findUser (username) {
    const user = await User.findOne({username: username})
    if(!user || user == undefined) {
      return Promise.reject ({
        statusCode: 401,
        statusMessage: 'User not found...'
      })
    }
    return user
  }

}

module.exports = GetUser
