'use strict';

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var

HashGenearator = /*#__PURE__*/function () {
  function HashGenearator() {_classCallCheck(this, HashGenearator);
    this.tokenSalt = 10;
  }_createClass(HashGenearator, null, [{ key: "genSalt", value:

    function genSalt(password) {var _this = this;
      return new Promise(function (resolve, reject) {
        _bcryptjs["default"].genSalt(_this.tokenSalt, function (err, salt) {
          if (err) {
            reject({ statusCode: 400, statusMessage: 'Unable to create account' });
          } else
          {
            resolve({ salt: salt, password: password });
          }
        });
      });
    } }, { key: "genHash", value:

    function genHash(salt, password) {
      return new Promise(function (resolve, reject) {
        _bcryptjs["default"].hash(password, salt, function (err, hash) {
          if (err) {
            reject({ statusCode: 400, statusMessage: 'Unable to create password' });
          } else {
            resolve({ salt: salt, password: password, hash: hash });
          }
        });
      });
    } }]);return HashGenearator;}();


module.exports = HashGenearator;
//# sourceMappingURL=passhash.js.map