'use strict';

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));
var _path = _interopRequireDefault(require("path"));
var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));

var _getuser = _interopRequireDefault(require("../helpers/getuser"));

var _authkey = require("../authkey");function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}


var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

VerifyToken = /*#__PURE__*/function () {
  function VerifyToken(pub) {_classCallCheck(this, VerifyToken);
    this.tokenKey = pub;
  }_createClass(VerifyToken, null, [{ key: "authorizeToken", value: function () {var _authorizeToken = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee3(req, res, next) {var parse, xAccessToken, checkTokenHeader, verifyToken, checkUser, confirmToken, decodedToken, validUser;return regeneratorRuntime.wrap(function _callee3$(_context3) {while (1) {switch (_context3.prev = _context3.next) {case 0:
                parse = req.headers;
                xAccessToken = parse['x-access-token'];

                checkTokenHeader = function checkTokenHeader(reqToken) {

                  if (reqToken) {
                    return reqToken;
                  }
                  return Promise.reject({
                    statusCode: 403,
                    statusMessage: 'No token provided...' });

                };

                verifyToken = /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(userToken) {var decoded;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (
                              _jsonwebtoken["default"].verify(userToken, _authkey.pub));case 2:decoded = _context.sent;if (!
                            decoded) {_context.next = 5;break;}return _context.abrupt("return",
                            decoded.username);case 5:return _context.abrupt("return",

                            Promise.reject({
                              statusCode: 401,
                              statusMessage: 'Failed to authenticate token...' }));case 6:case "end":return _context.stop();}}}, _callee);}));return function verifyToken(_x4) {return _ref.apply(this, arguments);};}();



                checkUser = /*#__PURE__*/function () {var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(username) {var user;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:_context2.next = 2;return (
                              _getuser["default"].findUser(username));case 2:user = _context2.sent;if (
                            user) {_context2.next = 5;break;}return _context2.abrupt("return",
                            Promise.reject({
                              statusCode: 403,
                              statusMessage: 'Authentication failed. User not found' }));case 5:return _context2.abrupt("return",


                            user);case 6:case "end":return _context2.stop();}}}, _callee2);}));return function checkUser(_x5) {return _ref2.apply(this, arguments);};}();_context3.prev = 5;_context3.next = 8;return (



                  checkTokenHeader(xAccessToken.toString()));case 8:confirmToken = _context3.sent;_context3.next = 11;return (
                  verifyToken(confirmToken));case 11:decodedToken = _context3.sent;_context3.next = 14;return (
                  checkUser(decodedToken));case 14:validUser = _context3.sent;if (!(
                validUser && req.jwtSession.id)) {_context3.next = 20;break;}
                log.info("Establish a verified a JWT session:" + req.jwtSession.encryptedUser.firstname + " " + req.jwtSession.encryptedUser.lastname + " is accessing the route with");
                log.info("Request JWT session data: ",
                req.jwtSession.id,
                req.jwtSession.claims,
                req.jwtSession.jwt);_context3.next = 22;break;case 20:_context3.next = 22;return (


                  Promise.reject({
                    statusCode: 401,
                    statusMessage: 'No session establish...' }));case 22:


                next();_context3.next = 30;break;case 25:_context3.prev = 25;_context3.t0 = _context3["catch"](5);



                log.error("".concat(_context3.t0.statusCode, " ").concat(_context3.t0.statusMessage));
                res.status(_context3.t0.statusCode).json({ success: false, message: _context3.t0.statusMessage });
                next(_context3.t0);case 30:case "end":return _context3.stop();}}}, _callee3, null, [[5, 25]]);}));function authorizeToken(_x, _x2, _x3) {return _authorizeToken.apply(this, arguments);}return authorizeToken;}() }]);return VerifyToken;}();





module.exports = VerifyToken;
//# sourceMappingURL=auth.js.map