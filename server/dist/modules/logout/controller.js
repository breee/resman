'use strict';

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));
var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));
var _user = _interopRequireDefault(require("../../models/user"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}

var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

UserLogout = /*#__PURE__*/function () {
  function UserLogout() {_classCallCheck(this, UserLogout);}_createClass(UserLogout, null, [{ key: "authLogout", value: function () {var _authLogout = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee(req, res, next) {var checkJwtSess, destroyJwtSess, token;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:

                checkJwtSess = function checkJwtSess() {

                  var parse = req.headers;var
                  xAccessToken = parse['x-access-token'];

                  if (!req.jwtSession.encryptedUser) {
                    return Promise.reject({
                      statusCode: 403,
                      statusMessage: 'Unauthorized access...' });

                  }
                  return xAccessToken.toString();
                };

                destroyJwtSess = function destroyJwtSess(verifiedToken) {
                  if (verifiedToken === req.jwtSession.jwt) {

                    req.jwtSession.destroy(function (err) {
                      if (err) throw err;

                      return res.status(200).json({ success: true, message: 'Successfully logout...' });
                    });
                  } else {
                    return Promise.reject({
                      statusCode: 401,
                      statusMessage: 'TokenMalformed found...' });

                  }
                };_context.prev = 2;_context.next = 5;return (



                  checkJwtSess());case 5:token = _context.sent;_context.next = 8;return (
                  destroyJwtSess(token));case 8:_context.next = 15;break;case 10:_context.prev = 10;_context.t0 = _context["catch"](2);


                log.error("".concat(_context.t0.statusCode, " ").concat(_context.t0.statusMessage));
                res.status(_context.t0.statusCode).json({ success: false, message: _context.t0.statusMessage });
                next(_context.t0);case 15:case "end":return _context.stop();}}}, _callee, null, [[2, 10]]);}));function authLogout(_x, _x2, _x3) {return _authLogout.apply(this, arguments);}return authLogout;}() }]);return UserLogout;}();






module.exports = UserLogout;
//# sourceMappingURL=controller.js.map