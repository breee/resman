'use strict';

var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));
var _mongoose = _interopRequireDefault(require("mongoose"));

var _item = _interopRequireDefault(require("../../models/item"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}

var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

GetItemQuantity = /*#__PURE__*/function () {
  function GetItemQuantity() {_classCallCheck(this, GetItemQuantity);}_createClass(GetItemQuantity, null, [{ key: "quantity", value: function () {var _quantity = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee2(req, res, next) {var itemId, getItemQuantity, itemQuantity;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:
                itemId = req.params.id;

                getItemQuantity = /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {var item;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (
                              _item["default"].findOne({ _id: itemId }));case 2:item = _context.sent;if (!(
                            !item || item == undefined)) {_context.next = 5;break;}return _context.abrupt("return",
                            Promise.reject({
                              statusCode: 401,
                              statusMessage: 'Items not found...' }));case 5:return _context.abrupt("return",


                            Promise.resolve(item));case 6:case "end":return _context.stop();}}}, _callee);}));return function getItemQuantity() {return _ref.apply(this, arguments);};}();_context2.prev = 2;_context2.next = 5;return (



                  getItemQuantity());case 5:itemQuantity = _context2.sent;
                res.json(itemQuantity.quantity);_context2.next = 14;break;case 9:_context2.prev = 9;_context2.t0 = _context2["catch"](2);

                log.error("".concat(_context2.t0.statusCode, " ").concat(_context2.t0.statusMessage));
                res.status(_context2.t0.statusCode).json({ success: false, message: _context2.t0.statusMessage });
                next(_context2.t0);case 14:case "end":return _context2.stop();}}}, _callee2, null, [[2, 9]]);}));function quantity(_x, _x2, _x3) {return _quantity.apply(this, arguments);}return quantity;}() }]);return GetItemQuantity;}();





module.exports = GetItemQuantity;
//# sourceMappingURL=get-quantity.js.map