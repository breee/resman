'use strict';

var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));
var _mongoose = _interopRequireDefault(require("mongoose"));

var _item = _interopRequireDefault(require("../../models/item"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}

var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

CreateItem = /*#__PURE__*/function () {
  function CreateItem() {_classCallCheck(this, CreateItem);
    this.name = '';
    this.description = '';
    this.imgUrl = '';
    this.quantity = '';
  }_createClass(CreateItem, null, [{ key: "sku", value: function () {var _sku = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee2(req, res, next) {var _this = this;var createItemSku;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:
                this.name = req.body.name;
                this.description = req.body.description;
                this.imgUrl = req.body.imgUrl;
                this.quantity = req.body.quantity;

                createItemSku = /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {var newItem, createItem;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:
                            newItem = new _item["default"]();
                            newItem.name = _this.name;
                            newItem.description = _this.description;
                            newItem.imgUrl = _this.imgUrl;
                            newItem.quantity = _this.quantity;_context.next = 7;return (

                              newItem.save());case 7:createItem = _context.sent;if (!
                            createItem) {_context.next = 10;break;}return _context.abrupt("return",
                            Promise.resolve(res.json({ success: true, message: 'Successfully created item...' })));case 10:return _context.abrupt("return",

                            Promise.reject({
                              statusCode: 400,
                              statusMessage: 'Unable to create item...' }));case 11:case "end":return _context.stop();}}}, _callee);}));return function createItemSku() {return _ref.apply(this, arguments);};}();_context2.prev = 5;_context2.next = 8;return (




                  createItemSku());case 8:_context2.next = 15;break;case 10:_context2.prev = 10;_context2.t0 = _context2["catch"](5);

                log.error("".concat(_context2.t0.statusCode, " ").concat(_context2.t0.statusMessage));
                res.status(_context2.t0.statusCode).json({ success: false, message: _context2.t0.statusMessage });
                next(_context2.t0);case 15:case "end":return _context2.stop();}}}, _callee2, this, [[5, 10]]);}));function sku(_x, _x2, _x3) {return _sku.apply(this, arguments);}return sku;}() }]);return CreateItem;}();





module.exports = CreateItem;
//# sourceMappingURL=add-item.js.map