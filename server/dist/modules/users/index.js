'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));
var _controller = require("./controller");
var _auth = _interopRequireDefault(require("../../config/auth"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var userRoute = require('node-async-router')();

userRoute.post('/add-user', _auth["default"].authorizeToken, _controller.RegisterUser.account);var _default =

userRoute;exports["default"] = _default;module.exports = exports.default;
//# sourceMappingURL=index.js.map