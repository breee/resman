'use strict';

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));
var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));

var _getuser = _interopRequireDefault(require("../../helpers/getuser"));
var _user = _interopRequireDefault(require("../../models/user"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}

var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

UserLogin = /*#__PURE__*/function () {
  function UserLogin() {_classCallCheck(this, UserLogin);
    this.username = '';
    this.password = '';
  }_createClass(UserLogin, null, [{ key: "authLogin", value: function () {var _authLogin = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee3(req, res, next) {var checkFields, checkUser, checkPassword, user, encryptedUser, isMatch, claims;return regeneratorRuntime.wrap(function _callee3$(_context3) {while (1) {switch (_context3.prev = _context3.next) {case 0:

                this.username = req.body.username || '';
                this.password = req.body.password || '';

                checkFields = function checkFields(username, password) {
                  if (username === '' || password === '') {
                    return Promise.reject({
                      statusCode: 401,
                      statusMessage: 'Unauthorized Login...' });

                  }
                  return username;
                };

                checkUser = /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(username) {var user;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (
                              _getuser["default"].findUser(username));case 2:user = _context.sent;return _context.abrupt("return",
                            user);case 4:case "end":return _context.stop();}}}, _callee);}));return function checkUser(_x4) {return _ref.apply(this, arguments);};}();


                checkPassword = /*#__PURE__*/function () {var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(authUser, reqPassword, encPassword) {var validPassword;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:_context2.next = 2;return (
                              authUser.comparePassword(reqPassword, encPassword));case 2:validPassword = _context2.sent;return _context2.abrupt("return",
                            validPassword);case 4:case "end":return _context2.stop();}}}, _callee2);}));return function checkPassword(_x5, _x6, _x7) {return _ref2.apply(this, arguments);};}();_context3.prev = 5;_context3.next = 8;return (




                  checkFields(this.username, this.password));case 8:user = _context3.sent;_context3.next = 11;return (
                  checkUser(user));case 11:encryptedUser = _context3.sent;_context3.next = 14;return (
                  checkPassword(encryptedUser, this.password, encryptedUser.password));case 14:isMatch = _context3.sent;
                delete encryptedUser.password;
                if (isMatch) {
                  req.jwtSession.encryptedUser = {
                    username: encryptedUser.username,
                    firstname: encryptedUser.firstname,
                    lastname: encryptedUser.lastname };


                  claims = {
                    iss: "my application name",
                    aud: "myapplication.com",
                    username: encryptedUser.username };


                  req.jwtSession.create(claims, function (error, token) {
                    log.info("Access Token Issued: ".concat(token));
                    res.json({
                      token: token,
                      firstname: encryptedUser.firstname,
                      lastname: encryptedUser.lastname });

                  });
                }_context3.next = 24;break;case 19:_context3.prev = 19;_context3.t0 = _context3["catch"](5);




                log.error("".concat(_context3.t0.statusCode, " ").concat(_context3.t0.statusMessage));
                res.status(_context3.t0.statusCode).json({ success: false, message: _context3.t0.statusMessage });
                next(_context3.t0);case 24:case "end":return _context3.stop();}}}, _callee3, this, [[5, 19]]);}));function authLogin(_x, _x2, _x3) {return _authLogin.apply(this, arguments);}return authLogin;}() }]);return UserLogin;}();






module.exports = UserLogin;
//# sourceMappingURL=controller.js.map