'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));
var _controller = _interopRequireDefault(require("./controller"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var loginRoute = require('node-async-router')();

loginRoute.post('/', _controller["default"].authLogin);var _default =

loginRoute;exports["default"] = _default;module.exports = exports.default;
//# sourceMappingURL=index.js.map