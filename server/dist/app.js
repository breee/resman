'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports["default"] = void 0;


var _dotenv = _interopRequireDefault(require("dotenv"));
var _express = _interopRequireDefault(require("express"));
var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));
var _path = _interopRequireDefault(require("path"));
var _morgan = _interopRequireDefault(require("morgan"));
var _jwtRedisSessionExtra = _interopRequireDefault(require("jwt-redis-session-extra"));
var _bodyParser = _interopRequireDefault(require("body-parser"));
var _bluebird = _interopRequireDefault(require("bluebird"));
var _mongoose = _interopRequireDefault(require("mongoose"));

var _db = _interopRequireDefault(require("./config/db"));

var _authkey = _interopRequireDefault(require("./authkey"));
var _auth = _interopRequireDefault(require("./config/auth"));

var _errorhandler = _interopRequireDefault(require("errorhandler"));

var _login = _interopRequireDefault(require("./modules/login"));
var _logout = _interopRequireDefault(require("./modules/logout"));
var _users = _interopRequireDefault(require("./modules/users"));
var _items = _interopRequireDefault(require("./modules/items"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}


var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var


pub = _authkey["default"].pub,priv = _authkey["default"].priv;


var app = (0, _express["default"])();


if (app.get('env') === 'production') _dotenv["default"].config({ encoding: 'latin1', path: './src/.env' });var


mongoClient = _db["default"].mongoClient,redisClient = _db["default"].redisClient,secretToken = _db["default"].secretToken;


_mongoose["default"].connect(mongoClient, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true });

_mongoose["default"].Promise = _bluebird["default"];



app.use(_express["default"]["static"](_path["default"].join(__dirname, '../../client/public')));


app.use((0, _morgan["default"])('dev'));


app.use(_bodyParser["default"].urlencoded({ extended: false }));
app.use(_bodyParser["default"].json());


app.use((0, _jwtRedisSessionExtra["default"])({
  client: redisClient,
  passphrase: secretToken,
  pubkey: pub,
  secret: priv,
  keyspace: "sess:",
  maxAge: 1800,
  algorithm: "RS256",
  requestKey: "jwtSession",
  requestArg: "accessToken" }));


app.all('*', function (req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, x-access-token, authorization');
  if ('OPTIONS' == req.method) return res.sendStatus(200);
  next();
});






app.use('/user', _users["default"]);
app.use('/item', _items["default"]);

app.use('/authenticate', _login["default"]);
app.use('/logout', _logout["default"]);



app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});




if (app.get('env') === 'development') {var

  errorNotification = function errorNotification(err, str, req) {
    var title = 'Error in ' + req.method + ' ' + req.url;
    log.error('Error in ' + req.method + ' ' + req.url);
  };app.use((0, _errorhandler["default"])({ log: errorNotification }));

  app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.send(401, 'invalid token...');
    }
  });
}



app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {} });

});var _default =

app;exports["default"] = _default;module.exports = exports.default;
//# sourceMappingURL=app.js.map