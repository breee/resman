"use strict";var _app = _interopRequireDefault(require("../src/app"));
var _http = _interopRequireDefault(require("http"));
var _debug = _interopRequireDefault(require("debug"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var debugClient = (0, _debug["default"])('resman:server');





var port = normalizePort(process.env.PORT || '3001');
_app["default"].set('port', port);






var server = _http["default"].createServer(_app["default"]);




server.listen(port, process.env.HOSTNAME || 'localhost');
server.on('error', onError);
server.on('listening', onListening);




function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {

    return val;
  }

  if (port >= 0) {

    return port;
  }

  return false;
}






function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ?
  'Pipe ' + port :
  'Port ' + port;


  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;}

}




function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ?
  'pipe ' + addr :
  'port ' + addr.port;
  debugClient('Listening on ' + bind + ' @ ' + addr.address);
}
//# sourceMappingURL=server.js.map