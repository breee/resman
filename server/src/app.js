'use strict'

//Environment modules
import dotenv          from 'dotenv'
import express         from 'express'
import sysLogger       from 'simple-node-logger'
import path            from 'path'
import logger          from 'morgan'
import JWTRedisSession from 'jwt-redis-session-extra'
import bodyParser      from 'body-parser'
import Promise         from 'bluebird'
import mongoose        from 'mongoose'
//Database config module
import databaseServer  from './config/db'
//Auth modules
import keychains       from './authkey'
import VerifyToken     from './config/auth'
//Errorhandler module
import errorhandler    from 'errorhandler'
//Routes
import loginRoute      from './modules/login'
import logoutRoute     from './modules/logout'
import usersRoute      from './modules/users'
import itemsRoute      from './modules/items'

//Server logger
const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

//Auth keys for server api access request
const { pub, priv } = keychains

//Express module instances
const app = express()

//Enviroment configuration variables
if (app.get('env') === 'production') dotenv.config({ encoding: 'latin1', path: './src/.env' })

//Database Nodejs clients
const { mongoClient, redisClient, secretToken } = databaseServer

//MongoDB config
mongoose.connect(mongoClient, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
})
mongoose.Promise = Promise

//static path config
// app.use(express.static(path.join(__dirname, '../../client/dist')))
app.use(express.static(path.join(__dirname, '../../client/public')))

//logger config
app.use(logger('dev'))

//bodyparser config
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

//JWT redis session config
app.use(JWTRedisSession({
  client    : redisClient,
  passphrase: secretToken,
  pubkey    : pub,
  secret    : priv,
  keyspace  : "sess:",
  maxAge    : 1800,
  algorithm : "RS256",
  requestKey: "jwtSession",
  requestArg: "accessToken"
}))

app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Credentials', true)
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH')
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, x-access-token, authorization')
  if ('OPTIONS' == req.method) return res.sendStatus(200)
  next()
})

// app.get('/*', (req, res) => {
//   // res.render(path.join(`${__dirname}../../../client/public/index.html`))
//   res.render(path.join(`${__dirname}../../../client/dist/index.html`))
// })

app.use('/user', usersRoute)
app.use('/item', itemsRoute)

app.use('/authenticate', loginRoute)
app.use('/logout', logoutRoute)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(errorhandler({log: errorNotification}))
  function errorNotification(err, str, req) {
    var title = 'Error in ' + req.method + ' ' + req.url
    log.error('Error in ' + req.method + ' ' + req.url)
  }

  app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.send(401, 'invalid token...')
    }
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

export default app
