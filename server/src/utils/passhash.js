'use strict'

import bcrypt from 'bcryptjs'

class HashGenearator {
  constructor() {
    this.tokenSalt = 10
  }

  static genSalt (password) {
    return new Promise ((resolve, reject) => {
      bcrypt.genSalt(this.tokenSalt, (err, salt) => {
        if (err) {
          reject({statusCode: 400, statusMessage: 'Unable to create account'})
        }
        else {
          resolve({salt: salt, password: password}) 
        }
      })
    })
  }

  static genHash (salt, password) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, salt, (err, hash) => {
        if (err) {
          reject({statusCode: 400, statusMessage: 'Unable to create password'})
        } else {
          resolve({salt: salt, password: password, hash: hash})
        }
      })
    })
  }
}

module.exports = HashGenearator