'use strict'

import bcrypt from 'bcryptjs'

class PassDecryptor {
  static async decrypt (reqPassword, userPassword) {
    const result = await bcrypt.compare(reqPassword, userPassword)
    if (!result) {
      return Promise.reject({
        statusCode: 401, 
        statusMessage: 'Login invalid...'
      })
    }
    return result
  }

}

module.exports = PassDecryptor