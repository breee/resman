'use strict'

import mongoose from 'mongoose'

const Schema = mongoose.Schema

const ItemSchema = new Schema({
  name: {
    type    : String,
    unique  : true,
    required: true
  },
  description: {
    type    : String,
    required: true
  },
  imgUrl : { type: String },
  quantity  : { type: Number },
  created_at: {
    type   : Date,
    default: Date.now
  },
  updated_at: { type: Date }
})

module.exports = mongoose.model('Item', ItemSchema)
