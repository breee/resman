'use strict'

import mongoose      from 'mongoose'
import HashGenerator from '../utils/passhash'
import PassDecryptor from '../utils/passdecrypt'

const Schema = mongoose.Schema

const UserSchema = new Schema({
  username: {
    type    : String,
    unique  : true,
    required: true
  },
  password: {
    type    : String,
    required: true
  },
  firstname : { type: String },
  lastname  : { type: String },
  created_at: {
    type   : Date,
    default: Date.now
  },
  updated_at: { type: Date }
})

UserSchema.pre('save', async function(next) {
  const user = this

  if (this.isModified('password') || this.isNew) {
    const saltResult     = await HashGenerator.genSalt(this.password)
    const hashedPassword = await HashGenerator.genHash(saltResult.salt,saltResult.password)

    user.password = hashedPassword.hash
  } else {
    // get the current date
    const currentDate = new Date()
    // change the updated_at field to current date
    this.updated_at = currentDate
    // if created_at doesn't exist, add to that field
    if (!this.created_at)
      this.created_at = currentDate

    // return next()
  }

})

UserSchema.methods.comparePassword = async (reqPassword, encPassword) => {
  const validatedPassword = await PassDecryptor.decrypt(reqPassword, encPassword)
  return validatedPassword
}


module.exports = mongoose.model('User', UserSchema)
