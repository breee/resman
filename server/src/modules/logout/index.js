'use strict'

import express   from 'express'
import UserLogout from './controller'

const logoutRoute = require('node-async-router')()

logoutRoute.get('/', UserLogout.authLogout)

export default logoutRoute

