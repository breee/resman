'use strict'

import bcrypt        from 'bcryptjs'
import sysLogger     from 'simple-node-logger'
import User          from '../../models/user'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class UserLogout {
  constructor () {}

  static async authLogout (req, res, next) {

    const checkJwtSess = () => {
      // let token = req.headers['x-access-token'] || req.headers.authorization
      const parse = req.headers
      const { 'x-access-token': xAccessToken } = parse

      if (!req.jwtSession.encryptedUser) {
        return Promise.reject({
          statusCode: 403,
          statusMessage: 'Unauthorized access...'
        })
      }
      return xAccessToken.toString()
    }

    const destroyJwtSess = (verifiedToken) => {
      if (verifiedToken === req.jwtSession.jwt) {
        //destroy session token from storage
        req.jwtSession.destroy(err => {
          if (err) throw err

          return res.status(200).json({success: true, message: 'Successfully logout...'})
        })
      } else {
        return Promise.reject({
          statusCode: 401,
          statusMessage: 'TokenMalformed found...'
        })
      }
    }

    try {

      const token = await checkJwtSess()
      await destroyJwtSess(token)

    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)

    }

  }
}

module.exports = UserLogout
