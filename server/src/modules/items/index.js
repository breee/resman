'use strict'

import express     from 'express'
import VerifyToken from '../../config/auth'

import { GetItems, CreateItem, GetItemQuantity, UpdateStock }   from './controller'

const itemRoute = require('node-async-router')()

itemRoute.get('/get-items', GetItems.sku)
itemRoute.get('/get-quantity/:id', VerifyToken.authorizeToken, GetItemQuantity.quantity)
itemRoute.put('/update-quantity/:id', UpdateStock.quantity)
itemRoute.post('/add-item', VerifyToken.authorizeToken, CreateItem.sku)

export default itemRoute
