'use strict'

import sysLogger from 'simple-node-logger'
import mongoose  from 'mongoose'

import Items     from '../../models/item'

const opts = { logFilePath:'sys.log', timestampFormat:'MMM.DD.YYYY HH:MM A' },
      log  = sysLogger.createSimpleLogger(opts)

class GetItems {
  constructor() {}

  static async sku (req, res, next) {

    const getItemSkus = async () => {
      const items = await Items.find({})
      if (!items || items == undefined) {
      return Promise.reject ({
          statusCode: 401,
          statusMessage: 'Items not found...'
        })
      }
      return Promise.resolve(items)
    }

    try {
      const listOfItems = await getItemSkus()
      res.json(listOfItems)
    } catch(err) {
      log.error(`${err.statusCode} ${err.statusMessage}`)
      res.status(err.statusCode).json({ success: false, message: err.statusMessage})
      next(err)
    }

  }
}

module.exports = GetItems
