'use strict'

import express     from 'express'
import UserLogin   from './controller'

const loginRoute = require('node-async-router')()

loginRoute.post('/', UserLogin.authLogin)

export default loginRoute

