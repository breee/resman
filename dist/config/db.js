'use strict';


var _redis = _interopRequireDefault(require("redis"));
var _mongoose = _interopRequireDefault(require("mongoose"));
var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var redisClient = _redis["default"].createClient({
  host: '127.0.0.1' || process.env.REDIS_HOST,
  port: 6379 || process.env.REDIS_PORT,
  url: null || process.env.REDIS_URL,
  password: null || process.env.REDIS_PASS }),

mongodb = _mongoose["default"].connection,
opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);

redisClient.on("error", function (err) {
  log.info('Redis Error ', err, ' occurred at ', new Date().toJSON());
});
redisClient.on('connect', function () {
  log.info('Redis initialized at ', new Date().toJSON());
});

mongodb.on('error', console.error.bind(console, 'Connection error: '));
mongodb.once('open', function () {
  log.info('MongoDB initialized at ', new Date().toJSON());
});

module.exports = {
  secretToken: process.env.PASSPHRASE || '137BAB574DRL6AA6A8MDD',
  redisClient: redisClient,

  mongoClient: 'mongodb://localhost:27017/resman' };
//# sourceMappingURL=db.js.map