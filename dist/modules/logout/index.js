'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));
var _controller = _interopRequireDefault(require("./controller"));
var _auth = _interopRequireDefault(require("../../config/auth"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var logoutRoute = require('node-async-router')();

logoutRoute.get('/', _auth["default"].authorizeToken, _controller["default"].authLogout);var _default =

logoutRoute;exports["default"] = _default;module.exports = exports.default;
//# sourceMappingURL=index.js.map