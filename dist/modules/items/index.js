'use strict';Object.defineProperty(exports, "__esModule", { value: true });exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));
var _auth = _interopRequireDefault(require("../../config/auth"));

var _controller = require("./controller");function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var itemRoute = require('node-async-router')();

itemRoute.get('/get-items', _controller.GetItems.sku);
itemRoute.get('/get-quantity/:id', _auth["default"].authorizeToken, _controller.GetItemQuantity.quantity);
itemRoute.put('/update-quantity/:id', _controller.UpdateStock.quantity);
itemRoute.post('/add-item', _auth["default"].authorizeToken, _controller.CreateItem.sku);var _default =

itemRoute;exports["default"] = _default;module.exports = exports.default;
//# sourceMappingURL=index.js.map