'use strict';

var _simpleNodeLogger = _interopRequireDefault(require("simple-node-logger"));
var _mongoose = _interopRequireDefault(require("mongoose"));

var _item = _interopRequireDefault(require("../../models/item"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}

var opts = { logFilePath: 'sys.log', timestampFormat: 'MMM.DD.YYYY HH:MM A' },
log = _simpleNodeLogger["default"].createSimpleLogger(opts);var

UpdateStock = /*#__PURE__*/function () {
  function UpdateStock() {_classCallCheck(this, UpdateStock);}_createClass(UpdateStock, null, [{ key: "quantity", value: function () {var _quantity = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(

      function _callee2(req, res, next) {var itemId, updatedQuantity, updateItemQuantity, updatedtemQuantity;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:
                itemId = req.params.id;
                updatedQuantity = req.body.data;
                console.log(itemId);


                updateItemQuantity = /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {var item, field, updateStock;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:
                            console.log('item');_context.next = 3;return (
                              _item["default"].findOne({ '_id': itemId }));case 3:item = _context.sent;

                            for (field in updatedQuantity) {
                              item[field] = updatedQuantity[field];
                            }_context.next = 7;return (

                              item.save());case 7:updateStock = _context.sent;if (!
                            updateStock) {_context.next = 13;break;}
                            console.log('Successfully updated stock...');return _context.abrupt("return",
                            Promise.resolve(updatedQuantity));case 13:return _context.abrupt("return",

                            Promise.reject({
                              statusCode: 400,
                              statusMessage: 'Unable to update stock...' }));case 14:case "end":return _context.stop();}}}, _callee);}));return function updateItemQuantity() {return _ref.apply(this, arguments);};}();_context2.prev = 4;_context2.next = 7;return (






                  updateItemQuantity());case 7:updatedtemQuantity = _context2.sent;
                res.json(updatedtemQuantity);_context2.next = 17;break;case 11:_context2.prev = 11;_context2.t0 = _context2["catch"](4);

                console.log('error update');
                log.error("".concat(_context2.t0.statusCode, " ").concat(_context2.t0.statusMessage));
                res.status(_context2.t0.statusCode).json({ success: false, message: _context2.t0.statusMessage });
                next(_context2.t0);case 17:case "end":return _context2.stop();}}}, _callee2, null, [[4, 11]]);}));function quantity(_x, _x2, _x3) {return _quantity.apply(this, arguments);}return quantity;}() }]);return UpdateStock;}();





module.exports = UpdateStock;
//# sourceMappingURL=update-item.js.map