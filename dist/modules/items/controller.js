"use strict";var _getItems = _interopRequireDefault(require("./get-items"));
var _addItem = _interopRequireDefault(require("./add-item"));
var _getQuantity = _interopRequireDefault(require("./get-quantity"));
var _updateItem = _interopRequireDefault(require("./update-item"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

module.exports = {
  GetItems: _getItems["default"],
  CreateItem: _addItem["default"],
  GetItemQuantity: _getQuantity["default"],
  UpdateStock: _updateItem["default"] };
//# sourceMappingURL=controller.js.map