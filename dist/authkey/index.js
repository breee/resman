'use strict';

var _fsExtra = _interopRequireDefault(require("fs-extra"));
var _path = _interopRequireDefault(require("path"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

module.exports = {
  pub: _fsExtra["default"].readFileSync(_path["default"].join(__dirname, 'pub.pem')),
  priv: _fsExtra["default"].readFileSync(_path["default"].join(__dirname, 'priv.pem')) };
//# sourceMappingURL=index.js.map