'use strict';

var _mongoose = _interopRequireDefault(require("mongoose"));
var _passhash = _interopRequireDefault(require("../utils/passhash"));
var _passdecrypt = _interopRequireDefault(require("../utils/passdecrypt"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}

var Schema = _mongoose["default"].Schema;

var UserSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true },

  password: {
    type: String,
    required: true },

  firstname: { type: String },
  lastname: { type: String },
  created_at: {
    type: Date,
    "default": Date.now },

  updated_at: { type: Date } });


UserSchema.pre('save', /*#__PURE__*/function () {var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(next) {var user, saltResult, hashedPassword, currentDate;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:
            user = this;if (!(

            this.isModified('password') || this.isNew)) {_context.next = 11;break;}_context.next = 4;return (
              _passhash["default"].genSalt(this.password));case 4:saltResult = _context.sent;_context.next = 7;return (
              _passhash["default"].genHash(saltResult.salt, saltResult.password));case 7:hashedPassword = _context.sent;

            user.password = hashedPassword.hash;_context.next = 14;break;case 11:


            currentDate = new Date();

            this.updated_at = currentDate;

            if (!this.created_at)
            this.created_at = currentDate;case 14:case "end":return _context.stop();}}}, _callee, this);}));return function (_x) {return _ref.apply(this, arguments);};}());






UserSchema.methods.comparePassword = /*#__PURE__*/function () {var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(reqPassword, encPassword) {var validatedPassword;return regeneratorRuntime.wrap(function _callee2$(_context2) {while (1) {switch (_context2.prev = _context2.next) {case 0:_context2.next = 2;return (
              _passdecrypt["default"].decrypt(reqPassword, encPassword));case 2:validatedPassword = _context2.sent;return _context2.abrupt("return",
            validatedPassword);case 4:case "end":return _context2.stop();}}}, _callee2);}));return function (_x2, _x3) {return _ref2.apply(this, arguments);};}();



module.exports = _mongoose["default"].model('User', UserSchema);
//# sourceMappingURL=user.js.map