'use strict';

var _mongoose = _interopRequireDefault(require("mongoose"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}

var Schema = _mongoose["default"].Schema;

var ItemSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: true },

  description: {
    type: String,
    required: true },

  imgUrl: { type: String },
  quantity: { type: Number },
  created_at: {
    type: Date,
    "default": Date.now },

  updated_at: { type: Date } });


module.exports = _mongoose["default"].model('Item', ItemSchema);
//# sourceMappingURL=item.js.map