'use strict';

var _mongoose = _interopRequireDefault(require("mongoose"));
var _user = _interopRequireDefault(require("../models/user"));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { "default": obj };}function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {try {var info = gen[key](arg);var value = info.value;} catch (error) {reject(error);return;}if (info.done) {resolve(value);} else {Promise.resolve(value).then(_next, _throw);}}function _asyncToGenerator(fn) {return function () {var self = this,args = arguments;return new Promise(function (resolve, reject) {var gen = fn.apply(self, args);function _next(value) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);}function _throw(err) {asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);}_next(undefined);});};}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var

GetUser = /*#__PURE__*/function () {function GetUser() {_classCallCheck(this, GetUser);}_createClass(GetUser, null, [{ key: "findUser", value: function () {var _findUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(
      function _callee(username) {var user;return regeneratorRuntime.wrap(function _callee$(_context) {while (1) {switch (_context.prev = _context.next) {case 0:_context.next = 2;return (
                  _user["default"].findOne({ username: username }));case 2:user = _context.sent;if (!(
                !user || user == undefined)) {_context.next = 5;break;}return _context.abrupt("return",
                Promise.reject({
                  statusCode: 401,
                  statusMessage: 'User not found...' }));case 5:return _context.abrupt("return",


                user);case 6:case "end":return _context.stop();}}}, _callee);}));function findUser(_x) {return _findUser.apply(this, arguments);}return findUser;}() }]);return GetUser;}();




module.exports = GetUser;
//# sourceMappingURL=getuser.js.map